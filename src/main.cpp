#include <climits>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <string.h>
#include <vector>

struct PcapHeader
{
    uint32_t magicNumber;
    uint16_t versionMajor;
    uint16_t versionMinor;
    int32_t  timezoneCorrectionSeconds;
    uint32_t timestampsSignificatFigures;
    uint32_t maxRecordDataLength;
    uint32_t linkLayerProtocol;
};

// Proccured from StackOverflow answer and prettified.
template <typename T>
T endianSwap(T value)
{
    static_assert (CHAR_BIT == 8, "CHAR_BIT != 8");

    union
    {
        T value;
        unsigned char bytes[sizeof(T)];
    } source, result;

    const int theAnswerToLifeTheUniverseAndEverything = 42;

    // No need to swap if we're already on big endian architecture.
    if (*(char *)&theAnswerToLifeTheUniverseAndEverything != theAnswerToLifeTheUniverseAndEverything)
    {
        
        return value;
    }

    source.value = value;
    size_t lastByteIndex = sizeof(T) - 1;

    for (size_t byteIndex = 0; byteIndex < sizeof(T); ++byteIndex)
    {
        result.bytes[byteIndex] = source.bytes[lastByteIndex - byteIndex];
    }

    return result.value;
}

class RecordParser
{
    public:
        RecordParser(uint32_t maxRecordDataLength);
        ~RecordParser();

        // Returns false on EOF.
        bool readRecord(std::ifstream &inputFile);
        // Returns empty string on failure.
        std::string parseDomainName();
    private:
        struct RecordHeader
        {
            uint32_t timestampSeconds;
            uint32_t timestampMicroseconds; // Nanoseconds for nanosecond precision pcap files.
            uint32_t actualLength;
            uint32_t originalLength;
        };

        RecordHeader recordHeader_;
        char *recordDataBuffer_;
};

RecordParser::RecordParser(uint32_t maxRecordDataLength)
{
    // TODO: Use dynamically sized buffer instead? Might use less space but
    // buffer allocations might have some impact on speed. Profile if required.
    recordDataBuffer_ = new char[maxRecordDataLength];
}

RecordParser::~RecordParser()
{
    delete[] recordDataBuffer_;
}

bool RecordParser::readRecord(std::ifstream &inputFile)
{
    inputFile.read((char *)&recordHeader_, sizeof(recordHeader_));

    if (inputFile.eof())
    {

        return false;
    }

    inputFile.read(recordDataBuffer_, recordHeader_.actualLength);
    
    if (inputFile.eof())
    {

        return false;
    }

    return true;
}

std::string RecordParser::parseDomainName()
{
    // TODO: Should we try to handle truncated records?
    if (recordHeader_.actualLength != recordHeader_.originalLength)
    {

        return "";
    }

    // Check if the frame is at least the size of a ethernet header.
    // Technically this will never happen since ethernet frames are padded to 64 bytes.
    uint32_t ethernetHeaderLength = 14;

    if (recordHeader_.actualLength < ethernetHeaderLength)
    {

        return "";
    }
    
    uint16_t internetLayerProtocol = *(uint16_t *)(recordDataBuffer_ + 12);
    internetLayerProtocol = endianSwap<uint16_t>(internetLayerProtocol);

    // TODO: Hope we don't have to deal with IEEE 802.2 ...
    // We only support IPv4 frames.
    if (internetLayerProtocol != 0x0800)
    {

        return "";
    }

    uint32_t ipv4HeaderStart = ethernetHeaderLength;
    uint32_t ipv4HeaderMinimumLength = 20;
    
    if (recordHeader_.actualLength < ipv4HeaderStart + ipv4HeaderMinimumLength)
    {

        return "";
    }
     
    // Lower 4 bit * 4
    uint32_t ipv4HeaderLength = (recordDataBuffer_[ipv4HeaderStart] & 0x15) * 4;

    if (recordHeader_.actualLength < ipv4HeaderStart + ipv4HeaderLength)
    {
        
        return "";
    }

    uint8_t transportLayerProtocol = recordDataBuffer_[ipv4HeaderStart + 9];

    // We only support UDP frames.
    if (transportLayerProtocol != 17)
    {

        return "";
    }

    uint32_t udpHeaderStart = ipv4HeaderStart + ipv4HeaderLength;
    uint32_t udpHeaderLength = 8;

    if (recordHeader_.actualLength < udpHeaderStart + udpHeaderLength)
    {

        return "";
    }

    uint16_t destinationPort = *(uint16_t *)(recordDataBuffer_ + udpHeaderStart + 2);
    destinationPort = endianSwap<uint16_t>(destinationPort);

    if (destinationPort != 53)
    {

        return "";
    }

    uint16_t udpTotalLength = *(uint16_t *)(recordDataBuffer_ + udpHeaderStart + 4);
    udpTotalLength = endianSwap<uint16_t>(udpTotalLength);

    if (recordHeader_.actualLength < udpHeaderStart + udpTotalLength)
    {

        return "";
    }

    uint32_t dnsQueryStart = udpHeaderStart + udpHeaderLength;
    uint32_t dnsHeaderLength = 12;

    if (recordHeader_.actualLength < dnsQueryStart + dnsHeaderLength)
    {

        return "";
    }

    // Heuristics for checking whether this is a dns query.
    uint16_t dnsFlags = *(uint16_t *)(recordDataBuffer_ + dnsQueryStart + 2);
    dnsFlags = endianSwap<uint16_t>(dnsFlags);
	uint16_t isResponse = dnsFlags >> 15;
    int responseCode = dnsFlags & 0x15;
    uint16_t questionCount = *(uint16_t *)(recordDataBuffer_ + dnsQueryStart + 4);
    questionCount = endianSwap<uint16_t>(questionCount);
    uint16_t answerCount = *(uint16_t *)(recordDataBuffer_ + dnsQueryStart + 8);
    answerCount = endianSwap<uint16_t>(answerCount);
    uint16_t authorityCount = *(uint16_t *)(recordDataBuffer_ + dnsQueryStart + 10);
    authorityCount = endianSwap<uint16_t>(authorityCount);

    if (isResponse || responseCode != 0 || questionCount != 1 ||
        answerCount != 0 || authorityCount != 0)
    {

        return "";
    }

    uint32_t domainNameStart = dnsQueryStart + dnsHeaderLength;
    uint32_t domainNameLength = udpTotalLength - udpHeaderLength - dnsHeaderLength - 4;
    uint32_t subdomainLength = 0;
    bool firstSubdomainLength = true;
    std::string domainName;

    for (uint32_t byteIndex = domainNameStart;
         byteIndex < domainNameStart + domainNameLength;
         ++byteIndex)
    {
        if (recordDataBuffer_[byteIndex] == '\0')
        {

            break;
        }

        if (subdomainLength == 0)
        {
            subdomainLength = recordDataBuffer_[byteIndex];

            if (firstSubdomainLength)
            {
                firstSubdomainLength = false;
            }
            else
            {
                domainName.push_back('.');
            }
        }
        else
        {
            subdomainLength--;
            domainName.push_back(recordDataBuffer_[byteIndex]);
        }
    }

    return domainName;
}

class PatternNode
{
    public:
        void createSubtree(const char *pattern, size_t patternLength);
        bool match(const char *name, size_t nameLength) const;
    private:
        std::map<char, PatternNode> subtrees;
};

void PatternNode::createSubtree(const char *pattern, size_t patternLength)
{
    if (patternLength == 0)
    {

        return;
    }

    char lastChar = pattern[patternLength - 1];

    if (subtrees.count(lastChar) == 0)
    {
        subtrees[lastChar] = PatternNode();
    }

    subtrees[lastChar].createSubtree(pattern, patternLength - 1);
}

bool PatternNode::match(const char *name, size_t nameLength) const
{
    // www.google.sk
    if (subtrees.empty())
    {
        
        return true;
    }

    char lastChar = name[nameLength - 1];

    if (subtrees.count(lastChar) == 1)
    {

        return subtrees.at(lastChar).match(name, nameLength - 1);
    }
    else
    {

        return false;
    }
}

void parsePcapFile(const char *filePath, std::set<std::string> &domainNames)
{
    std::cerr << "Parsing pcap file: " << filePath << std::endl;
    std::ifstream inputFile(filePath, std::ios::binary|std::ios::in);

    if (!inputFile)
    {
        std::cerr << "Failed to open the file!" << filePath << " !" << std::endl;
        
        return;
    }

    PcapHeader header;
    inputFile.read((char *)&header, sizeof(header));

    if (inputFile.eof())
    {
        std::cerr << "Failed to read pcap file header!" << std::endl;

        return;
    }

    // TODO: Handle non-native byte ordering.
    // TODO: Handle nanosecond precission pcap files.
    if (header.magicNumber != 0xa1b2c3d4)
    {
        std::cerr << "Wrong magic number!" << std::endl;
        
        return;
    }

    if (header.versionMajor != 2 || header.versionMinor != 4)
    {
        std::cerr << "Unsupported pcap file version!" << std::endl;
        
        return;
    }

    if (header.linkLayerProtocol != 1) // LINKTYPE_ETHERNET
    {
        std::cerr << "Unsupported link layer protocol!" << std::endl;
        
        return;
    }

    // TODO: See if we need to check other pcap header values.
    RecordParser recordParser(header.maxRecordDataLength);
    int frames = 0;

    while (recordParser.readRecord(inputFile))
    {
        std::string domainName = recordParser.parseDomainName();

        if (!domainName.empty())
        {
            domainNames.insert(domainName);
        }
        frames++;
    }

    //std::cerr << "Parsed " << frames << " frames." << std::endl;
}

void parseWhitelistFile(char *filePath, std::vector<std::string> &patterns)
{
    std::cerr << "Parsing whitelist file: " << filePath << std::endl;
    std::ifstream inputFile(filePath, std::ios::binary|std::ios::in);

    if (!inputFile)
    {
        std::cerr << "Failed to open the file!" << filePath << " !" << std::endl;
        
        return;
    }

    for (std::string line; getline(inputFile, line); )
    {
        patterns.push_back(line);
    }
}

void parseWhitelistFile(char *filePath, PatternNode &patternTree)
{
    std::cerr << "Parsing whitelist file: " << filePath << std::endl;
    std::ifstream inputFile(filePath, std::ios::binary|std::ios::in);

    if (!inputFile)
    {
        std::cerr << "Failed to open the file!" << filePath << " !" << std::endl;
        
        return;
    }

    for (std::string line; getline(inputFile, line); )
    {
        patternTree.createSubtree(&line[1], line.size() - 1);
    }
}

std::vector<std::string> filterMatchingDomainNames(std::set<std::string> domainNames,
                                                   std::vector<std::string> whitelistPatterns)
{
    std::vector<std::string> result;

    for (const std::string &name : domainNames)
    {
        for (const std::string &pattern : whitelistPatterns)
        {
            // *.sk 3
            // www.google.sk 13
            size_t startingIndex = name.length() - pattern.length();
            const char *haystack = &name[startingIndex];
            const char *needle = &pattern[1];

            if (strstr(haystack, needle))
            {
                result.push_back(name);
            }
        }
    }

    return result;
}

std::vector<std::string> filterMatchingDomainNames(std::set<std::string> domainNames,
                                                   PatternNode &patternTree)
{
    std::vector<std::string> result;

    for (const std::string &name : domainNames)
    {
        if (patternTree.match(&name[0], name.size()))
        {
            result.push_back(name);
        }
    }

    return result;
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cerr << "Arguments must be in format: path_to_pcap_file path_to_whitelist_file" << std::endl;
        
        return 1;
    }

    std::set<std::string> domainNames;
    parsePcapFile(argv[1], domainNames);
    std::vector<std::string> whitelistPatterns;
    parseWhitelistFile(argv[2], whitelistPatterns);
    std::vector<std::string> matchedDomainNames = filterMatchingDomainNames(domainNames, whitelistPatterns);
    
    for (const auto &domainName : matchedDomainNames)
    {
        std::cout << domainName << std::endl;
    }

    std::cout << std::endl;
    PatternNode whitelistTree;
    parseWhitelistFile(argv[2], whitelistTree);
    matchedDomainNames = filterMatchingDomainNames(domainNames, whitelistTree);

    for (const auto &domainName : matchedDomainNames)
    {
        std::cout << domainName << std::endl;
    }

    return 0;
}
